class fizzbuzz:
    def __init__(self,words=['fizz', 'buzz']):
        self.words = words
        self.primes = self.__find_primes(len(words))

    def __is_prime(self, number):
        if number > 1:
            for num in range(2, number // 2 + 1):
                if number % num == 0:
                    return False
            return True
        return False

    def __find_primes(self, number):
        primes=[3,5]
        while len(primes) < number:
            for c in range(primes[-1]+1, primes[-1]*2-1):
                if self.__is_prime(c):
                    primes.append(c)
                    break
        return primes

    def fb_single(self,value):
        ret_str = ''
        idx = 0
        for pr in self.primes:
            if value % pr == 0:
                ret_str += self.words[idx]
            idx += 1
        if not ret_str:
            ret_str = str(value)
        return ret_str

    def fb_range(self,number):
        numbers = []
        for val in range(1, number+1):
            numbers.append(self.fb_single(val))
        return numbers
