### FizzBuzz module for Python

[FizzBuzz](https://rosettacode.org/wiki/FizzBuzz) on WIkipedia.

This module can generate a sequence of numbers with any number of strings as replacement for greater than 3 prime divisors of numbers.

Initialize the module wit a list of words you want replaced. Say, `['fizz','buzz','bang','pow']`. The module will now replace the primes 3,5,7 and 11 by the corresponding words, concatenating them when a number is multiple than more than one prime.

Calling the method `fb_single`, will return the FizzBuzz string for that number only. Calling `fb_range` will return a list, starting at 1 and ending with the number provided.

For the list of terms above, calling `fb_range(120)` will produce:

```
['1', '2', 'fizz', '4', 'buzz', 'fizz', 'bang', '8', 'fizz', 'buzz', 'pow', 'fizz', '13',
 'bang', 'fizzbuzz', '16', '17', 'fizz', '19', 'buzz', 'fizzbang', 'pow', '23', 'fizz',
 'buzz', '26', 'fizz', 'bang', '29', 'fizzbuzz', '31', '32', 'fizzpow', '34', 'buzzbang',
 'fizz', '37', '38', 'fizz', 'buzz', '41', 'fizzbang', '43', 'pow', 'fizzbuzz', '46', '47',
 'fizz', 'bang', 'buzz', 'fizz', '52', '53', 'fizz', 'buzzpow', 'bang', 'fizz', '58', '59',
 'fizzbuzz', '61', '62', 'fizzbang', '64', 'buzz', 'fizzpow', '67', '68', 'fizz', 'buzzbang',
 '71', 'fizz', '73', '74', 'fizzbuzz', '76', 'bangpow', 'fizz', '79', 'buzz', 'fizz', '82',
 '83', 'fizzbang', 'buzz', '86', 'fizz', 'pow', '89', 'fizzbuzz', 'bang', '92', 'fizz', '94',
 'buzz', 'fizz', '97', 'bang', 'fizzpow', 'buzz', '101', 'fizz', '103', '104', 'fizzbuzzbang',
 '106', '107', 'fizz', '109', 'buzzpow', 'fizz', 'bang', '113', 'fizz', 'buzz', '116', 'fizz',
 '118', 'bang', 'fizzbuzz']
```
